import React, { Component } from "react";
import styles from "../ProductCard/Cards.module.scss";
import Button from "../button/button";
import favoIcon from "../icons/favorite.png";
import notFavoIcon from "../icons/notFavorite.png";
import PropTypes from "prop-types";

class CardItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: {},
    };
  }
  openModal = (modalId) => {
    this.setState({ openedModalId: modalId });
  };
  render() {
    const {
      name,
      imgUrl,
      price,
      article,
      favourite,
      addToCart,
      setModalProps,
      addToFavourites,
    } = this.props;
    return (
      <div className={styles.card}>
        <button type="button" className={styles.favoButton}>
          <img
            onClick={() => {
              setModalProps({ article });
              addToFavourites(article);
            }}
            src={favourite ? favoIcon : notFavoIcon}
            alt="Favourite"
            style={{ maxWidth: "30px" }}
          />
        </button>
        <span className={styles.title}>{name}</span>
        <img className={styles.itemAvatar} src={imgUrl} alt={name} />
        <span className={styles.description}>Ціна: {price} грн.</span>

        <div className={styles.btnContainer}>
          <div>
            <Button
              background="green"
              text="Add to cart"
              onClick={() => {
                setModalProps({ article, name });
                addToCart(article);
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CardItem;

CardItem.propTypes = {
  name: PropTypes.string,
  imgUrl: PropTypes.string,
  price: PropTypes.number,
  article: PropTypes.number,
  color: PropTypes.string,
  favourite: PropTypes.bool,
  setModalProps: PropTypes.func,
  addToFavourites: PropTypes.func,
};
