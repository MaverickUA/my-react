import { Component } from "react";
import styles from "./Header.module.scss";
import cartIcon from "../icons/cart.png";
import favoIcon from "../icons/favorite.png";
class Header extends Component {
  render() {
    let cartQt;
    let favouriteQt;
    if (localStorage.getItem("carts")) {
      cartQt = <span>{JSON.parse(localStorage.getItem("carts")).length}</span>;
    } else {
      cartQt = <span>0</span>;
    }

    if (localStorage.getItem("favourites")) {
      favouriteQt = (
        <span>{JSON.parse(localStorage.getItem("favourites")).length}</span>
      );
    } else {
      favouriteQt = <span>0</span>;
    }

    return (
      <header className={styles.root}>
        <h1>BEST ONLINE SHOPPING HERE!</h1>
        <ul>
          <li>
            <img
              src={cartIcon}
              alt="Cart"
              style={{ maxWidth: "30px", cursor: "pointer" }}
            />
          </li>
          <li>{cartQt}</li>
        </ul>
        <ul>
          <li>
            <img
              src={favoIcon}
              alt="Cart"
              style={{ maxWidth: "30px", cursor: "pointer" }}
            />
          </li>
          <li>{favouriteQt}</li>
        </ul>
      </header>
    );
  }
}

export default Header;
