import React, { Component } from "react";
import styles from "../ProductList/List.module.scss";
import Card from "../ProductCard/ProductCard";

class CardList extends Component {
  render() {
    const { addToCart, cards, setModalProps, addToFavourites, favourites } =
      this.props;
    return (
      <div>
        <ul className={styles.list}>
          {cards.map(({ name, imgUrl, price, article, isFavourite }) => (
            <li key={article}>
              <Card
                favourite={favourites.includes(article)}
                addToCart={addToCart}
                article={article}
                name={name}
                price={price}
                imgUrl={imgUrl}
                isFavourite={isFavourite}
                setModalProps={setModalProps}
                addToFavourites={addToFavourites}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default CardList;
