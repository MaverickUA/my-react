import { Component } from "react";
import Button from "../button/button.jsx";
import styles from "../modal/Modal.module.scss";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.modalWindowDeclarations = [
      {
        id: "modal1",
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it wont be possible to undo this action. Are you sure you want to delete it?",
        btnType: "secondary",
        btnText1: "Ok",
        btnText2: "Cancel",
      },
      {
        id: "modal2",
        header: "Make the right choice",
        text: "Are you sure you want add this product to the cart?",
        btnType: "action",
        btnText1: "Yes!",
        btnText2: "No",
      },
    ];
    this.state = {
      modal: {},
    };
  }
  componentDidMount() {
    const modalToShow = this.modalWindowDeclarations.find(
      (modal) => modal.id === this.props.openedModal
    );
    this.setState({ modal: modalToShow });
  }
  openModal = (modalId) => {
    this.setState({ openedModalId: modalId });
  };

  closeModal = () => {
    this.setState({ openedModalId: "" });
  };
  aproveModal = () => {
    alert("Done!");
    this.props.closeModal();
  };
  render() {
    return (
      <div
        className={styles.main}
        onClick={(e) => e.currentTarget === e.target && this.props.closeModal()}
      >
        <div className={styles.modalWrapper}>
          <div className={styles.modalHeader}>
            {this.state.modal.header ? (
              <h3>{this.state.modal.header}</h3>
            ) : null}
            <Button type="small" text="X" onClick={this.props.closeModal} />
          </div>
          <div className={styles.modalText}>
            <p>{this.state.modal.text}</p>
          </div>
          <div className={styles.modalFooter}>
            <Button
              type={this.state.modal.btnType}
              text={this.state.modal.btnText1}
              onClick={this.aproveModal}
            />
            <Button
              type={this.state.modal.btnType}
              text={this.state.modal.btnText2}
              onClick={this.props.closeModal}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
