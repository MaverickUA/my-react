import React, { Component } from "react";
import Header from "./components/Header/Header";
import CardList from "./components/ProductList/ProductList";

class App extends Component {
  state = {
    favourites: [],
    cards: [],
    carts: [],
    modalProps: {},
  };

  setModalProps = (value) => {
    this.setState({ modalProps: value });
  };

  async componentDidMount() {
    const cards = await fetch(`./items.json`).then((res) => res.json());
    cards.forEach((card) => {
      card.isFavourite =
        localStorage.getItem("Favourites") &&
        JSON.parse(localStorage.getItem("Favourites")).includes(card.article);
    });
    this.setState({ cards });
    if (localStorage.getItem("carts")) {
      const carts = await JSON.parse(localStorage.getItem("carts"));
      this.setState({ carts });
    }
    if (localStorage.getItem("favourites")) {
      const favourites = await JSON.parse(localStorage.getItem("favourites"));
      this.setState({ favourites });
    }
  }

  openFirstModal = () => {
    this.setState({ firstModalDisplay: true });
  };

  closeFirstModal = () => {
    this.setState({ firstModalDisplay: false });
  };

  addToCart = (article) => {
    this.setState((current) => {
      const carts = [...current.carts];
      const index = carts.findIndex((el) => el.article === article);

      if (index === -1) {
        carts.push({ ...article, count: 1 });
      } else {
        carts[index].count += 1;
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      return { carts };
    });
  };

  addToFavourites = (article) => {
    this.setState((current) => {
      let favourites = [...current.favourites];
      if (current.favourites.includes(article)) {
        favourites = current.favourites.filter((el) => {
          return el !== article;
        });
      } else {
        favourites = [...current.favourites, article];
      }
      localStorage.setItem("favourites", JSON.stringify(favourites));
      this.closeFirstModal();
      return { favourites };
    });
  };

  render() {
    const { cards, carts, modalProps, favourites } = this.state;

    return (
      <>
        <Header carts={carts} favourites={favourites} />
        <main>
          <section>
            <h1>Каталог</h1>
            <CardList
              favourites={favourites}
              addToCart={this.addToCart}
              cards={cards}
              carts={carts}
              setModalProps={this.setModalProps}
              addToFavourites={this.addToFavourites}
            />
          </section>
        </main>
      </>
    );
  }
}

export default App;
