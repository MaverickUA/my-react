import React from "react";
import CardList from "../components/ProductList/ProductList";

const MainPage = () => {
  return (
    <main>
      <section>
        <h2>Catalog</h2>
        <CardList />
      </section>
    </main>
  );
};

export default MainPage;
