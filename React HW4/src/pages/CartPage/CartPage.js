import React from "react";
import CardItem from "../../components/ProductCard/ProductCard";
import styles from "./CartPage.module.scss";
import { getCart } from "../../store/actions";
import { useSelector } from "react-redux";

const CartPage = () => {
  const items = useSelector((state) => state.items.data);
  const cart = items.filter((item) => item.inCartAmount);

  if (!getCart()) {
    return <h2 className="container">Empty</h2>;
  }

  return (
    <>
      <h2>Cart</h2>
      <div className={styles.cart}>
        {cart.map((item) => {
          return <CardItem key={item.article} item={item} fromCart />;
        })}
      </div>
    </>
  );
};

export default CartPage;
