import React from "react";
import { useEffect } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./AppRoutes.js";
import { loadItems } from "./store/actions";
import { useDispatch, useSelector } from "react-redux";
import { ADD_TO_CART, REMOVE_FROM_CART, CLOSE_MODAL } from "./store/types";

const App = () => {
  const isLoading = useSelector((state) => state.items.isLoading);
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modal.isOpen);
  const thisItem = useSelector((state) => state.modal.data);
  const infoModal = useSelector((state) => state.modal.info);

  const closeModal = () => {
    dispatch({ type: CLOSE_MODAL });
  };

  const addToCart = (data) => {
    dispatch({ type: ADD_TO_CART, payload: data });
  };

  const removeFromCart = (data) => {
    dispatch({ type: REMOVE_FROM_CART, payload: data });
  };

  useEffect(() => {
    dispatch(loadItems());
  }, [dispatch]);

  if (isLoading) {
    return <div className="App">Loading... Don`t go anywhere</div>;
  }

  return (
    <BrowserRouter>
      <>
        <Header />
        <section>
          <AppRoutes />
        </section>

        {isOpen && (
          <Modal
            closeButton={true}
            closeModal={closeModal}
            actions={[
              <Button
                key={1}
                onClick={() => {
                  if (infoModal === "Add to cart") {
                    addToCart(thisItem.article);
                  }
                  if (infoModal === "Remove from cart") {
                    removeFromCart(thisItem.article);
                  }
                  closeModal();
                }}
                className="modal-button"
                text="Ok"
              />,
              <Button
                key={2}
                onClick={closeModal}
                className="modal-button"
                text="Cancel"
              />,
            ]}
          />
        )}
      </>
    </BrowserRouter>
  );
};

export default App;
