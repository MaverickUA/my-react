export const SUCCESS = "SUCCESS";
export const SHOW_MODAL = "SHOW_MODAL";
export const ADD_REMOVE_FAVORITE = "ADD_REMOVE_FAVORITE";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
