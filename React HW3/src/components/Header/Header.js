import styles from "./Header.module.scss";
import cartIcon from "../icons/cart.png";
import favoIcon from "../icons/favorite.png";
import { NavLink } from "react-router-dom";

function Header({ cartQt, favouriteQt }) {
  localStorage.getItem("carts")
    ? (cartQt = <span>{JSON.parse(localStorage.getItem("carts")).length}</span>)
    : (cartQt = <span>0</span>);

  localStorage.getItem("favourites")
    ? (favouriteQt = (
        <span>{JSON.parse(localStorage.getItem("favourites")).length}</span>
      ))
    : (favouriteQt = <span>0</span>);

  return (
    <header className={styles.root}>
      <ul>
        <li>
          <NavLink to="/">Home</NavLink>
        </li>
        <li>
          <NavLink to="/cart">
            Cart
            <img
              src={cartIcon}
              alt="Cart"
              style={{ maxWidth: "30px", cursor: "pointer" }}
            />
          </NavLink>
          {cartQt}
        </li>
        <li>
          <NavLink to="/favourites">
            Favourites
            <img
              src={favoIcon}
              alt="Favourites"
              style={{ maxWidth: "30px", cursor: "pointer" }}
            />
            {favouriteQt}
          </NavLink>
        </li>
      </ul>
    </header>
  );
}

export default Header;
