import styles from "../ProductCard/Cards.module.scss";
import Button from "../button/button";
import favoIcon from "../icons/favorite.png";
import notFavoIcon from "../icons/notFavorite.png";
import PropTypes from "prop-types";

function CardItem({
  item,
  favourite,
  openModal,
  setModalProps,
  addToFavourites,
  toCart,
  fromCart,
}) {
  const { name, imgUrl, price, article } = item;
  return (
    <div className={styles.card}>
      <button type="button" className={styles.favoButton}>
        <img
          onClick={() => {
            setModalProps(article);
            addToFavourites(article);
          }}
          src={favourite ? favoIcon : notFavoIcon}
          alt="Favourite"
          style={{ maxWidth: "30px" }}
        />
      </button>
      <span className={styles.title}>{name}</span>
      <img className={styles.itemAvatar} src={imgUrl} alt={name} />
      <span className={styles.description}>Ціна: {price} грн.</span>

      <div className={styles.btnContainer}>
        <div>
          {toCart && (
            <Button
              backgroundColor="green"
              text="Add to cart"
              onClick={() => {
                setModalProps({ article, name });
                openModal("Add to cart");
              }}
            />
          )}
          {fromCart && (
            <Button
              background="red"
              onClick={() => {
                setModalProps({ article, name });
                openModal("Remove from cart");
              }}
              text={"X"}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default CardItem;

CardItem.propTypes = {
  name: PropTypes.string,
  imgUrl: PropTypes.string,
  price: PropTypes.number,
  article: PropTypes.string,
  favourite: PropTypes.bool,
  addToCart: PropTypes.func,
  openModal: PropTypes.func,
  setModalProps: PropTypes.func,
  addToFavourites: PropTypes.func,
};
