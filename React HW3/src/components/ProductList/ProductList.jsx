import styles from "../ProductList/List.module.scss";
import CardItem from "../ProductCard/ProductCard";

function CardList({
  addToCart,
  cards,
  openModal,
  setModalProps,
  addToFavourites,
  favourites,
}) {
  return (
    <div>
      <ul className={styles.list}>
        {cards.map((item) => (
          <li key={item.article}>
            <CardItem
              favourite={favourites.includes(item.article)}
              addToCart={addToCart}
              item={item}
              openModal={openModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              toCart
            />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default CardList;
