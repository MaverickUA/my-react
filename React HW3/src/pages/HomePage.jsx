import CardList from "../components/ProductList/ProductList";

function HomePage({
  favourites,
  addToCart,
  cards,
  carts,
  openModal,
  setModalProps,
  addToFavourites,
}) {
  return (
    <main>
      <section>
        <h2 style={{ textAlign: "center" }}>Store items</h2>
        <CardList
          favourites={favourites}
          addToCart={addToCart}
          cards={cards}
          carts={carts}
          openModal={openModal}
          setModalProps={setModalProps}
          addToFavourites={addToFavourites}
        />
      </section>
    </main>
  );
}

export default HomePage;
