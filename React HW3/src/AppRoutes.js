import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";
import CartPage from "./pages/CartPage/CartPage";
import FavouritesPage from "./pages/FavouritesPage/FavouritesPage";

const AppRoutes = ({
  favourites,
  addToCart,
  cards,
  carts,
  openModal,
  setModalProps,
  addToFavourites,
  deleteThisItem,
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openModal={openModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
          />
        }
      />

      <Route
        path="/cart"
        element={
          <CartPage
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openModal={openModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            deleteThisItem={deleteThisItem}
          />
        }
      />

      <Route
        path="/favourites"
        element={
          <FavouritesPage
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openModal={openModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            deleteThisItem={deleteThisItem}
          />
        }
      />

      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
};

export default AppRoutes;
