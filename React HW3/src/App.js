import { useState, useEffect } from "react";
import "./App.css";
import Header from "./components/Header/Header";
import Button from "./components/button/button";
import Modal from "./components/modal/modal";
import AppRoutes from "./AppRoutes";
import { BrowserRouter } from "react-router-dom";

const App = () => {
  const [modalDisplay, setModalDisplay] = useState(false);
  const [favourites, setFavourites] = useState([]);
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [modalProps, setModalProps] = useState({});
  const [modalAction, setModalAction] = useState("Add to cart");

  useEffect(() => {
    (async () => {
      const cards = await fetch(`./items.json`).then((res) => res.json());
      cards.forEach((card) => {
        card.isFavourite =
          localStorage.getItem("Favourites") &&
          JSON.parse(localStorage.getItem("Favourites")).includes(card.article);
      });
      setCards(cards);
      if (localStorage.getItem("carts")) {
        const carts = await JSON.parse(localStorage.getItem("carts"));
        setCarts(carts);
      }
      if (localStorage.getItem("favourites")) {
        const favourites = await JSON.parse(localStorage.getItem("favourites"));
        setFavourites(favourites);
      }
    })();
  }, []);

  const openModal = (text) => {
    if (text === "Add to cart") {
      setModalAction("Add to cart");
    }
    if (text === "Remove from cart") {
      setModalAction("Remove from cart");
    }
    setModalDisplay(true);
  };

  const closeModal = () => {
    setModalDisplay(false);
  };

  const addToCart = (article) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex((el) => el === article);

      if (index === -1) {
        carts.push(...article);
      } else {
        carts[index].count += 1;
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeModal();
      return carts;
    });
  };

  const deleteThisItem = (article) => {
    setCarts((current) => {
      const carts = [...current];

      const index = carts.findIndex((el) => el === article);

      if (index !== -1) {
        carts.splice(index, 1);
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeModal();
      return carts;
    });
  };

  const addToFavourites = (article) => {
    setFavourites((current) => {
      let favourites = [...current];
      if (current.includes(article)) {
        favourites = current.filter((el) => {
          return el !== article;
        });
      } else {
        favourites = [...current, article];
      }
      localStorage.setItem("favourites", JSON.stringify(favourites));
      closeModal();
      return favourites;
    });
  };

  return (
    <BrowserRouter>
      <>
        <Header carts={carts} favourites={favourites} />
        <main>
          <section>
            <AppRoutes
              deleteThisItem={deleteThisItem}
              favourites={favourites}
              addToCart={addToCart}
              cards={cards}
              carts={carts}
              openModal={openModal}
              setModalProps={setModalProps}
              addToFavourites={addToFavourites}
              modalAction={modalAction}
            />
          </section>
          {modalDisplay && (
            <Modal
              text={`${modalAction} '${modalProps.name}'?`}
              className="modal"
              modalDisplay={modalDisplay}
              closeButton={true}
              close={closeModal}
              modalProps={modalProps}
              addToCart={addToCart}
              setModalProps={setModalProps}
              actions={[
                <Button
                  key={1}
                  onClick={() => {
                    if (modalAction === "Add to cart") {
                      addToCart(modalProps.article);
                    }
                    if (modalAction === "Remove from cart") {
                      deleteThisItem(modalProps.article);
                    }
                  }}
                  className="modal-button"
                  text="Yes"
                  // type="action"
                  // background="green"
                />,
                <Button
                  key={2}
                  onClick={closeModal}
                  className="modal-button"
                  text="Cancel"
                  // background="green"
                />,
              ]}
            />
          )}
        </main>
      </>
    </BrowserRouter>
  );
};

export default App;
