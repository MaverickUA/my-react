import { Formik, Form, Field, ErrorMessage } from "formik";
import Button from "../../components/Button/Button";
import styles from "./forms.module.scss";
import * as yup from "yup";
import { getCart, setCart } from "../../store/actions";
import { MAKE_ORDER, REMOVE_FROM_CART } from "../../store/types";
import { useDispatch, useSelector } from "react-redux";

const Forms = () => {
  const validationSchema = yup.object().shape({
    name: yup
      .string()
      .min(3, "Min 3 symbols")
      .required("Name field is required"),
    surname: yup
      .string()
      .min(3, "Min 3 symbols")
      .required("Surname field is required"),
    age: yup.number().required("Age field is required").positive().integer(),
    adress: yup.string().required("Adress field is required"),
    phone: yup.number().required("Phone field is required").positive(),
  });
  const dispatch = useDispatch();
  const removeFromCart = (data) => {
    dispatch({ type: REMOVE_FROM_CART, payload: data });
  };
  const makeOrder = (data) => {
    dispatch({ type: MAKE_ORDER, payload: data });
  };
  const items = useSelector((state) => state.items.data);
  return (
    <Formik
      initialValues={{
        name: "",
        surname: "",
        age: "",
        adress: "",
        phone: "",
      }}
      onSubmit={(values, { resetForm }) => {
        const data = getCart();
        const order = items.filter((item) => item.inCartAmount);
        makeOrder(order);
        Object.keys(data).forEach((element) => {
          removeFromCart(element);
        });
        alert("Order completed! Thank you!");
        console.log(order);
        setCart({});
        console.log(values);
        resetForm();
      }}
      validationSchema={validationSchema}
    >
      <Form>
        <Field type="text" name="name" placeholder="Name" />
        <ErrorMessage name="name">
          {(msg) => <span className={styles.error}>{msg}</span>}
        </ErrorMessage>

        <Field type="text" name="surname" placeholder="Surname" />
        <ErrorMessage name="surname">
          {(msg) => <span className={styles.error}>{msg}</span>}
        </ErrorMessage>

        <Field type="text" name="age" placeholder="Age" />
        <ErrorMessage name="age">
          {(msg) => <span className={styles.error}>{msg}</span>}
        </ErrorMessage>

        <Field type="text" name="adress" placeholder="Adress" />
        <ErrorMessage name="adress">
          {(msg) => <span className={styles.error}>{msg}</span>}
        </ErrorMessage>

        <Field type="phone" name="phone" placeholder="Phone" />
        <ErrorMessage name="phone">
          {(msg) => <span className={styles.error}>{msg}</span>}
        </ErrorMessage>

        <Button type="submit" text="Checkout"></Button>
      </Form>
    </Formik>
  );
};

export default Forms;
