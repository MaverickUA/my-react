import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import Modal from "../Modal/Modal";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

jest.mock("react-redux", () => ({
  useSelector: () => {
    return { info: "testAction", data: { name: "itemName" } };
  },
}));

test("should component exist", () => {
  render(<Modal />);
  const modal = screen.getByTestId("modal");
  expect(modal).toBeInTheDocument();
  expect(modal).not.toHaveTextContent("Ok");
  expect(modal).toContainHTML("div");
});

test("Close Modal", () => {
  const closeModal = jest.fn();
  render(<Modal closeModal={closeModal} />);
  fireEvent.click(screen.getByTestId("modal"));
  expect(closeModal).toHaveBeenCalled();
});
test("Snapshot", () => {
  const { container } = render(<Modal />);
  expect(container.innerHTML).toMatchSnapshot();
});
