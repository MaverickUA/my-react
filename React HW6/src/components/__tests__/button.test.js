import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import Button from "../Button/Button";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("should component exist", () => {
  render(<Button />);
  const button = screen.getByTestId("buttonJest");
  expect(button).toBeInTheDocument();
  expect(button).not.toHaveTextContent("Ababagalamaga");
  expect(button).not.toContainHTML("div");
});

const handleClick = jest.fn();

test("should onClick work", () => {
  render(
    <Button backgroundColor="#b3382c" text="Ok" onClick={handleClick}></Button>
  );
  fireEvent.click(screen.getByText("Ok"));
  expect(handleClick).toHaveBeenCalled();
});

test("Snapshot", () => {
  const { container } = render(<Button />);
  expect(container.innerHTML).toMatchSnapshot();
});
