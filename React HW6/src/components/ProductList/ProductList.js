import React, { useContext } from "react";
import CardItem from "../ProductCard/ProductCard";
import styles from "../ProductList/List.module.scss";
import { useSelector } from "react-redux";
import { AppContext } from "../Context/AppContext";
import Button from "../Button/Button";

const CardList = () => {
  const cards = useSelector((state) => state.items.data);
  const { tableView, changeTypeView } = useContext(AppContext);
  const viewType = tableView ? styles.table : styles.list;
  return (
    <div>
      <Button
        backgroundColor="orange"
        onClick={() => changeTypeView(tableView)}
        text={tableView ? "Go to Cards view" : "Go to Table view"}
      ></Button>
      <ul className={viewType}>
        {cards.map((item) => (
          <li key={item.imgUrl}>
            <CardItem
              classname={styles.card}
              key={item.imgUrl}
              item={item}
              toCart
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CardList;
