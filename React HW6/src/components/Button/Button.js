import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

const Button = ({ backgroundColor, text, onClick }) => {
  return (
    <button
      data-testid="buttonJest"
      className={styles.modalButton}
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  text: "",
  onClick: () => {},
};
