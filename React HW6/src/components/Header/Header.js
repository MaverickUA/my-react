import styles from "./Header.module.scss";
import cartIcon from "../icons/cart.png";
import favoIcon from "../icons/favorite.png";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

const Header = () => {
  const items = useSelector((state) => state.items.data);
  const favourites = items.filter((item) => item.favourite);
  const cartItems = items.filter((item) => item.inCartAmount);

  let cartQt;
  let favouriteQt;
  if (cartItems.length) {
    cartQt = <span>{cartItems.length}</span>;
  } else {
    cartQt = <span>0</span>;
  }

  if (favourites.length) {
    favouriteQt = <span>{favourites.length}</span>;
  } else {
    favouriteQt = <span>0</span>;
  }

  return (
    <header className={styles.root}>
      <ul>
        <li>
          <NavLink to="/">MAIN</NavLink>
        </li>
        <li>
          <NavLink to="/cart">
            Cart
            <img
              src={cartIcon}
              alt="Cart"
              style={{ maxWidth: "30px", cursor: "pointer" }}
            />
          </NavLink>
          {cartQt}
        </li>
        <li>
          <NavLink to="/favourites">
            Favourites
            <img src={favoIcon} alt="Cart" width={26} />
            {favouriteQt}
          </NavLink>
        </li>
      </ul>
    </header>
  );
};

export default Header;
