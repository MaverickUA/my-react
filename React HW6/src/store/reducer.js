import {
  SUCCESS,
  SHOW_MODAL,
  ADD_REMOVE_FAVORITE,
  CLOSE_MODAL,
  ADD_TO_CART,
  REMOVE_FROM_CART,
  MAKE_ORDER,
} from "./types";
import { getCart, setCart, getFavorites, setFavorites } from "./actions";
import produce from "immer";

const initialState = {
  items: {
    data: [],
    isLoading: true,
  },
  modal: {
    info: "",
    data: null,
    isOpen: false,
  },
  order: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS:
      return {
        ...state,
        items: { ...state.data, data: action.payload, isLoading: false },
      };
    case SHOW_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          info: action.payload.info,
          data: action.payload.data,
          isOpen: true,
        },
      };
    case CLOSE_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          isOpen: false,
        },
      };

    case ADD_REMOVE_FAVORITE:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.favourite = !el.favourite;

            const favourites = getFavorites();
            if (favourites.includes(el.article)) {
              favourites.splice(favourites.indexOf(el.article), 1);
            } else {
              favourites.push(el.article);
            }
            setFavorites(favourites);
          }
          return el;
        });
      });

    case ADD_TO_CART:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inCartAmount++;
            const cart = getCart();
            if (Object.keys(cart).includes(el.article)) {
              cart[el.article]++;
            } else {
              cart[el.article] = 1;
            }
            setCart(cart);
          }
          return el;
        });
      });

    case REMOVE_FROM_CART:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inCartAmount--;
            const cart = getCart();
            cart[el.article]--;
            if (cart[el.article] === 0) {
              delete cart[el.article];
            }
            setCart(cart);
          }
          return el;
        });
      });
    case MAKE_ORDER:
      return produce(state, (draftState) => {
        draftState.order = action.payload;
      });

    default:
      return state;
  }
};

export default reducer;
