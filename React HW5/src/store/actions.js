import { SUCCESS } from "./types";

export const getCart = () => {
  const cart = localStorage.getItem("Cart");
  return cart ? JSON.parse(cart) : {};
};

export const setCart = (data) => {
  localStorage.setItem("Cart", JSON.stringify(data));
};

export const getFavorites = () => {
  const favouritesLS = localStorage.getItem("Favourite");
  return favouritesLS ? JSON.parse(favouritesLS) : [];
};

export const setFavorites = (data) => {
  localStorage.setItem("Favourite", JSON.stringify(data));
};

export const loadItems = () => async (dispatch) => {
  const favourites = getFavorites();
  const cart = getCart();

  try {
    const data = await fetch(`./items.json`).then((res) => res.json());

    const newItems = data.map((item) => {
      item.favourite = favourites.includes(item.article);
      item.inCartAmount = cart[item.article] || null;
      return item;
    });
    dispatch({ type: SUCCESS, payload: newItems });
  } catch (error) {
    console.log(error);
  }
};
