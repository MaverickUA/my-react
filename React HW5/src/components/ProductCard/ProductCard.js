import styles from "./Cards.module.scss";
import Button from "../Button/Button";
import favoIcon from "../icons/favorite.png";
import notFavoIcon from "../icons/notFavorite.png";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { SHOW_MODAL, ADD_REMOVE_FAVORITE } from "../../store/types";

const CardItem = ({ item, toCart, fromCart }) => {
  const { name, imgUrl, article, price, favourite } = item;

  const dispatch = useDispatch();

  const addToFavourite = (id) => {
    dispatch({ type: ADD_REMOVE_FAVORITE, payload: id });
  };

  const showModal = (info, data) => {
    dispatch({ type: SHOW_MODAL, payload: { info, data } });
  };

  return (
    <div className={styles.card}>
      <button type="button" className={styles.favoButton}>
        <img
          onClick={() => {
            addToFavourite(article);
          }}
          src={favourite ? favoIcon : notFavoIcon}
          alt="Favourite"
          style={{ maxWidth: "30px" }}
        />
      </button>
      <span className={styles.title}>{name}</span>
      <img className={styles.itemAvatar} src={imgUrl} alt={name} />
      <span className={styles.description}>Ціна: {price} грн.</span>

      <div className={styles.btnContainer}>
        <div>
          {toCart && (
            <Button
              backgroundColor="green"
              text="Add to cart"
              onClick={() => {
                showModal("Add to cart", item);
              }}
            />
          )}
          {fromCart && (
            <Button
              backgroundColor="red"
              onClick={() => {
                showModal("Remove from cart", item);
              }}
              text={"X"}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardItem;

CardItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    favourite: PropTypes.bool,
  }),
  toCart: PropTypes.bool,
  fromCart: PropTypes.bool,
};
