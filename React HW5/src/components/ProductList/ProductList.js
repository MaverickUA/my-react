import React from "react";
import CardItem from "../ProductCard/ProductCard";
import styles from "../ProductList/List.module.scss";
import { useSelector } from "react-redux";

const CardList = () => {
  const cards = useSelector((state) => state.items.data);

  return (
    <div>
      <ul className={styles.list}>
        {cards.map((item) => (
          <li key={item.imgUrl}>
            <CardItem key={item.imgUrl} item={item} toCart />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CardList;
