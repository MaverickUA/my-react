import React, { Component } from "react";
import Button from "./components/button/button";
import Modal from "./components/modal/modal";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openedModalId: "",
    };
  }
  openModal = (modalId) => {
    this.setState({ openedModalId: modalId });
  };
  closeModal = () => {
    this.setState({ openedModalId: "" });
  };

  render() {
    return (
      <div className="btnWrapper">
        <Button
          name="Button"
          text="Open first modal"
          background="green"
          onClick={() => this.openModal("modal1")}
        />
        <Button
          name="Button"
          text="Open second modal"
          background="blue"
          onClick={() => this.openModal("modal2")}
        />

        {this.state.openedModalId ? (
          <Modal
            openedModal={this.state.openedModalId}
            closeModal={this.closeModal}
          />
        ) : null}
      </div>
    );
  }
}

export default App;
