import { Component } from "react";
import styles from "../button/Button.module.scss";

class Button extends Component {
  render() {
    const { background, text, onClick, type } = this.props;
    let buttonClass;
    switch (type) {
      case "small":
        buttonClass = styles.btnSmall;
        break;
      case "secondary":
        buttonClass = styles.btnSecondary;
        break;
      case "action":
        buttonClass = styles.actionBtn;
        break;
      default:
        buttonClass = styles.btnPrimary;
    }
    return (
      <button
        className={buttonClass}
        style={{ background: background }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;
